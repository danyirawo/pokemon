<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PokemonController;

Route::get('/Pokemon', [PokemonController::class, 'index']);
Route::get('/Pokemon/Paginar', [PokemonController::class, 'indexPaginado'])->name('paginar_pokemon');


