<h3>PokeDesk</h3> 

Búsqueda y recopilación de información para capturar Pokemones.

## Requisitos

- Docker
- Laravel
- Mysql

## Comandos para el Docker

Para el contenedor del proyecto:<br>
enlace de archivo Dockerfile para contenedor del proyecto <br> (https://documentos-digitales-std.s3.amazonaws.com/adjuntos/9iCb0Collk4z2EoTcrFEvRfWp91uxDQPSotrNZox.txt)
- docker build -t pokemon .
- docker run -dp 8080:80 --name pokemon -v /home/djimenez/desarrollo/pokemon/pokemon:/var/www/html/pokemon pokemon

Para el contenedor de la base de datos:
- docker volume create mysqlpokemon
- docker run -dp 33066:3306 --name mysqlpokemon -e MYSQL_ROOT_PASSWORD=123456 --mount src=mysqlpokemon,dst=/var/lib/mysql mysql:latest

NOTA: recuerde cambiar la ruta de sus carpetas y credenciales de los comandos.

## Después de Clonación del Proyecto

Debera realizar el pull desde la rama master, ejecutar las migraciones y el comando para el llenar la base de datos:

- git pull origin master
- php artisan migrate
- php artisan Guardar:pokemon
