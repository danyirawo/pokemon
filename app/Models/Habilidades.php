<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Habilidades extends Model
{
    use HasFactory;
    protected $connection = 'mysql';
    protected $table = 'habilidades';
    protected $fillable = ['nombre', 'pokemon_id'];
    protected $primarykey = 'id';

    public function pokemon()
	{
		$relacion = $this->belongsTo(Pokemon::class, 'pokemon_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Pokemon::class, 'pokemon_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

}