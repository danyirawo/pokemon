<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetallesPokemon extends Model
{
    use HasFactory;
    protected $connection = 'mysql';
    protected $table = 'detalles_pokemon';
    protected $fillable = ['pokemon_id', 'experiencia_base', 'altura', 'peso', 'imagen'];
    protected $primarykey = 'id';

    public function pokemon()
	{
		$relacion = $this->belongsTo(Pokemon::class, 'pokemon_id', 'id');
		if($relacion){
			$relacion = $this->belongsTo(Pokemon::class, 'pokemon_id', 'id');
		}else{
			$relacion = null;

		}

		return $relacion;
	}

}