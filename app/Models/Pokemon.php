<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{
    use HasFactory;
    protected $connection = 'mysql';
    protected $table = 'pokemon';
    protected $fillable = ['nombre'];
    protected $primarykey = 'id';

    public function detallepokemon()
    {
        $relacion = $this->belongsTo(DetallesPokemon::class, 'id', 'pokemon_id');
		if($relacion){
			$relacion = $this->belongsTo(DetallesPokemon::class, 'id', 'pokemon_id');
		}else{
			$relacion = null;

		}

		return $relacion;
    }

    public function habilidad()
    {
        return $this->hasMany(Habilidades::class,'pokemon_id', 'id');
    }
   

}