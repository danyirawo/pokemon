<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Models\Pokemon;
use App\Models\DetallesPokemon;
use App\Models\Habilidades;
use Http, DB;
use Illuminate\Pagination\Paginator;



class PokemonController extends Controller
{

    public function index(Request $request)
    {
        $dataCompleta = [];
        $paginasiguiente = null;
        $paginaAterior = null;
        
          try {
            $client = new Client();
            $request = $client->get("https://pokeapi.co/api/v2/pokemon/?offset=0&limit=10" );
            $response = $request->getBody()->getContents();
            $data =  json_decode($response);
            $paginasiguiente = $data->next;
            $paginaAterior = $data->previous;
            $data = $data->results;

                foreach($data as $key => $value){
                    $client2 = new Client(); 
                    $requestDetalle = $client2->get($value->url);
                    $response2 = $requestDetalle->getBody()->getContents();
                    $detalles =  json_decode($response2); 
                    $habilidades = $detalles->abilities;
                    $dataCompleta[] = ['nombre' => $value->name , 'detalles' => $detalles];
                    
                }
            
            return view('pokemon')
            ->with('paginasiguiente', $paginasiguiente)
            ->with('paginaAterior', $paginaAterior) 
            ->with('dataCompleta', $dataCompleta);  

          }catch (\Throwable $th) {
            //$response = $e->getResponse();

            $Pokemones = Pokemon::paginate(10);

            return view('pokemon2')
            ->with('Pokemones', $Pokemones); 
        }  
    }

    public function indexPaginado(Request $request)
    {
        //dd($request);
        $dataCompleta = [];
        $paginasiguiente = null;
        $paginaAterior = null;
        if($request->s){
            $urlSiguienteAnterior = $request->url_s;
        }
        if($request->a){
            $urlSiguienteAnterior = $request->url_a;
        }
        
          try {
            $client = new Client();
            $request = $client->get($urlSiguienteAnterior);
            $response = $request->getBody()->getContents();
            $data =  json_decode($response);
            $paginasiguiente = $data->next;
            $paginaAterior = $data->previous;
            $data = $data->results;

                foreach($data as $key => $value){
                    $client2 = new Client(); 
                    $requestDetalle = $client2->get($value->url);
                    $response2 = $requestDetalle->getBody()->getContents();
                    $detalles =  json_decode($response2); 
                    $habilidades = $detalles->abilities;
                    $dataCompleta[] = ['nombre' => $value->name , 'detalles' => $detalles];
                   
                }  
            
            return view('pokemon')
            ->with('paginasiguiente', $paginasiguiente)
            ->with('paginaAterior', $paginaAterior) 
            ->with('dataCompleta', $dataCompleta);  

        }catch (\Throwable $th) {
            //$response = $e->getResponse();

            $Pokemones = Pokemon::paginate(10);

            return view('pokemon2')
            ->with('Pokemones', $Pokemones); 
        } 
     
    }


    public function gurdarPokemones()
    {
        $limpiarTablaPokemon = 'TRUNCATE TABLE pokemon.pokemon;';
        $limpiarDatosPokemon = DB::connection('mysql')->select($limpiarTablaPokemon);

        $limpiarTablaDetallesPokemon = 'TRUNCATE TABLE pokemon.detalles_pokemon;';
        $limpiarDatosDetallesPokemon = DB::connection('mysql')->select($limpiarTablaDetallesPokemon);

        $limpiarTablaHabilidades = 'TRUNCATE TABLE pokemon.habilidades;';
        $limpiarDatosHabilidades = DB::connection('mysql')->select($limpiarTablaHabilidades);
    

        $dataCompleta = [];
        
          try {
            $client = new Client();
            $request = $client->get("https://pokeapi.co/api/v2/pokemon/?offset=0&limit=100");
            $response = $request->getBody()->getContents();
            $data =  json_decode($response);
            $data = $data->results;

                foreach($data as $key => $value){
                    $client2 = new Client(); 
                    $requestDetalle = $client2->get($value->url);
                    $response2 = $requestDetalle->getBody()->getContents();
                    $detalles =  json_decode($response2); 
                    $habilidades = $detalles->abilities;
                    $dataCompleta[] = ['nombre' => $value->name , 'detalles' => $detalles];
                    
                    $nuevoPokemon = new Pokemon();
                    $nuevoPokemon->nombre = $value->name;
                    $nuevoPokemon->save();

                    $nuevoDetallePokemon = new DetallesPokemon();
                    $nuevoDetallePokemon->pokemon_id = $nuevoPokemon->id;
                    $nuevoDetallePokemon->experiencia_base = $detalles->base_experience;
                    $nuevoDetallePokemon->altura = $detalles->height;
                    $nuevoDetallePokemon->peso = $detalles->weight;
                    $nuevoDetallePokemon->imagen = $detalles->sprites->front_default;
                    $nuevoDetallePokemon->save();

                        foreach ($habilidades as $key2 => $value2) {
                            $nombreHabilidad = $value2->ability->name;
                            $nuevaHabilidad = new Habilidades();
                            $nuevaHabilidad->pokemon_id = $nuevoPokemon->id;
                            $nuevaHabilidad->nombre = $nombreHabilidad;
                            $nuevaHabilidad->save();
                        }  
                }
            
            }catch (\Throwable $th) {
                $response = $e->getResponse();
            }
    }
    
}