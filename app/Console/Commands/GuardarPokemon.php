<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\PokemonController;

class GuardarPokemon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Guardar:pokemon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Guardar 100 Pokémones en la Base Datos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $act =  new  PokemonController();
        $act->gurdarPokemones();
    }
}
