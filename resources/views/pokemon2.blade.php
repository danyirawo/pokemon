<!DOCTYPE html>
<html>
<head>
    <title>Pokémon</title>
    <link rel="shortcut icon" href="{{ asset('icons.png') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>  
    <link rel="stylesheet" href="https://oculux.nsdbytes.com/laravel/public/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://oculux.nsdbytes.com/laravel/public/assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://oculux.nsdbytes.com/laravel/public/assets/vendor/animate-css/vivify.min.css">
    <link rel="stylesheet" href="https://oculux.nsdbytes.com/laravel/public/assets/vendor/c3/c3.min.css">
    <link rel="stylesheet" href="https://oculux.nsdbytes.com/laravel/public/assets/vendor/chartist/css/chartist.css">
    <link rel="stylesheet" href="https://oculux.nsdbytes.com/laravel/public/assets/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
    <link rel="stylesheet" href="https://oculux.nsdbytes.com/laravel/public/assets/vendor/toastr/toastr.min.css">
    <link rel="stylesheet" href="https://oculux.nsdbytes.com/laravel/public/assets/css/site.min.css">   
    <style type="text/css">
    .modal{
        background-color: transparent;
        position:fixed;
        top:0;
        right:0;
        bottom:0;
        left: 800px;
        opacity: transparent;
        pointer-events:none;
        transition: all 1s;
    }
    </style>
    <style>
    .bg-white {
        background-color: #28a745!important;
        color: rgba(255,255,255,0.7);
    }
    </style>
</head>
<body>
<div class="container-fluid">
<div class="block-header">
    <div class="row clearfix">
        <div class="col-md-6 col-sm-12">
            <h3><img style="width: 50px; height: 50px;" src="{{ asset('icons.png') }}"  alt=""/> Pokémon</h3>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-lg-6 col-md-12 col-sm-12">
        <div class="row clearfix">
        @if(count($Pokemones)>0)
        @foreach ($Pokemones as $indice => $datos)
            <div class="col-lg-4 col-6">
                <div class="card-wrapper flip-left">
                    <div class="card s-widget-top">
                        <div class="front  px-4" style="color:#17a2b8;">
                        <div style="text-transform: uppercase;"><b>{{$datos->nombre}}</b></div>
                            <div class="py-4 m-0 h2 text-info"><center><img style="width: 115px; height: 115px;" src="{{$datos->detallepokemon->imagen}}"  alt=""/></center></div>
                                <div class="d-flex">
                            </div>
                        </div>
                        <div class="back px-4 text-info">
                            <p class="text-center" style="text-transform: uppercase;"><b>Detalles</b></p>
                            <li class="text-light" style="font-size:12px;"><b>Experiencia:</b> {{$datos->detallepokemon->experiencia_base}}</li>
                            <li class="text-light" style="font-size:12px;"><b>Altura:</b> {{$datos->detallepokemon->altura}}</li>
                            <li class="text-light" style="font-size:12px;"><b>Peso:</b> {{$datos->detallepokemon->peso}}</li>
                            <li class="text-light" style="font-size:12px;"><b>Habilidades:</b> 
                            @foreach($datos->habilidad as $indice2 => $habilidades)
                                {{$habilidades->nombre}}
                            @endforeach
                            </li>
                            <a class="pull-right" data-toggle="modal" data-target=".poke_{{$indice}}">Ver</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        @endif
        </div>
    </div>
    @foreach ($Pokemones as $indice => $datos)
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="modal fade poke_{{$indice}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" style="width: 700px; height: 500px;">
                <div class="modal-content">
                <div class="modal-header">
                        <h3 style="text-transform: uppercase; color:#17a2b8;">{{$datos->nombre}}</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="color:#17a2b8;">&times;</span>
                        </button>
                    </div>
                <div class="text-center">
                        <div><center><img style="width: 250px; height: 250px;" src="{{$datos->detallepokemon->imagen}}"  alt=""/></center></div>
                        <h3 style="text-transform: uppercase; color:#17a2b8;">Detalles de {{$datos->nombre}}</h3>
                        <div class="back px-4 text-info">
                            <p style="font-size:18px;"><b>Experiencia:</b> {{$datos->detallepokemon->experiencia_base}}</p>
                            <p style="font-size:18px;"><b>Altura:</b> {{$datos->detallepokemon->altura}}</p>
                            <p style="font-size:18px;"><b>Peso:</b> {{$datos->detallepokemon->peso}}</p>
                            <p style="font-size:18px;"><b>Habilidades:</b> 
                            @foreach($datos->habilidad as $indice2 => $habilidades)
                                {{$habilidades->nombre}}
                            @endforeach
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach 
</div>


<div class="col-sm-12">
    <div class="text-center">

        {{ $Pokemones->links() }}

    </div>
</div>
</div>
<script type="text/javascript">
    function mostrarPoke() {
        document.getElementById('datopoke').style.display = 'block';
    }
</script>
</body>
</html>
